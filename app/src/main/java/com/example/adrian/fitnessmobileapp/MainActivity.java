package com.example.adrian.fitnessmobileapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.*;
import android.widget.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    //Monday Variables
    public Button MondayAddWorkout, MondayAddMeal;
    public ListView MondayWorkoutsView, MondayMealsView;
    private ArrayList<String> MondayWorkoutsArray, MondayMealsArray;
    private ArrayAdapter<String> MondayWorkoutsAdapter, MondayMealsAdapter;

    //Tuesday Variables
    public Button TuesdayAddWorkout, TuesdayAddMeal;
    public ListView TuesdayWorkoutsView, TuesdayMealsView;
    private ArrayList<String> TuesdayWorkoutsArray, TuesdayMealsArray;
    private ArrayAdapter<String> TuesdayWorkoutsAdapter, TuesdayMealsAdapter;

    //Wednesday Variables
    public Button WednesdayAddWorkout, WednesdayAddMeal;
    public ListView WednesdayWorkoutsView, WednesdayMealsView;
    private ArrayList<String> WednesdayWorkoutsArray, WednesdayMealsArray;
    private ArrayAdapter<String> WednesdayWorkoutsAdapter, WednesdayMealsAdapter;

    //Thursday Variables
    public Button ThursdayAddWorkout, ThursdayAddMeal;
    public ListView ThursdayWorkoutsView, ThursdayMealsView;
    private ArrayList<String> ThursdayWorkoutsArray, ThursdayMealsArray;
    private ArrayAdapter<String> ThursdayWorkoutsAdapter, ThursdayMealsAdapter;

    //Friday Variables
    public Button FridayAddWorkout, FridayAddMeal;
    public ListView FridayWorkoutsView, FridayMealsView;
    private ArrayList<String> FridayWorkoutsArray, FridayMealsArray;
    private ArrayAdapter<String> FridayWorkoutsAdapter, FridayMealsAdapter;

    //Saturday Variables
    public Button SaturdayAddWorkout, SaturdayAddMeal;
    public ListView SaturdayWorkoutsView, SaturdayMealsView;
    private ArrayList<String> SaturdayWorkoutsArray, SaturdayMealsArray;
    private ArrayAdapter<String> SaturdayWorkoutsAdapter, SaturdayMealsAdapter;

    //Sunday Variables
    public Button SundayAddWorkout, SundayAddMeal;
    public ListView SundayWorkoutsView, SundayMealsView;
    private ArrayList<String> SundayWorkoutsArray, SundayMealsArray;
    private ArrayAdapter<String> SundayWorkoutsAdapter, SundayMealsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar appToolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(appToolbar);

        //Monday Buttons
        MondayAddWorkout = (Button) findViewById(R.id.MondayAddWorkout);
        MondayAddMeal = (Button) findViewById(R.id.MondayAddMeal);

        //Tuesday Buttons
        TuesdayAddWorkout = (Button) findViewById(R.id.TuesdayAddWorkout);
        TuesdayAddMeal = (Button) findViewById(R.id.TuesdayAddMeal);

        //Wednesday Buttons
        WednesdayAddWorkout = (Button) findViewById(R.id.WednesdayAddWorkout);
        WednesdayAddMeal = (Button) findViewById(R.id.WednesdayAddMeal);

        //Thursday Buttons
        ThursdayAddWorkout = (Button) findViewById(R.id.ThursdayAddWorkout);
        ThursdayAddMeal = (Button) findViewById(R.id.ThursdayAddMeal);

        //Friday Buttons
        FridayAddWorkout = (Button) findViewById(R.id.FridayAddWorkout);
        FridayAddMeal = (Button) findViewById(R.id.FridayAddMeal);

        //Saturday Buttons
        SaturdayAddWorkout = (Button) findViewById(R.id.SaturdayAddWorkout);
        SaturdayAddMeal = (Button) findViewById(R.id.SaturdayAddMeal);

        //Sunday Buttons
        SundayAddWorkout = (Button) findViewById(R.id.SundayAddWorkout);
        SundayAddMeal = (Button) findViewById(R.id.SundayAddMeal);

        //Monday ListViews
        MondayWorkoutsView = (ListView) findViewById(R.id.MondayWorkoutsEntries);
        MondayMealsView = (ListView) findViewById(R.id.MondayMealsEntries);

        //Tuesday ListViews
        TuesdayWorkoutsView = (ListView) findViewById(R.id.TuesdayWorkoutsEntries);
        TuesdayMealsView = (ListView) findViewById(R.id.TuesdayMealsEntries);

        //Wednesday ListViews
        WednesdayWorkoutsView = (ListView) findViewById(R.id.WednesdayWorkoutsEntries);
        WednesdayMealsView = (ListView) findViewById(R.id.WednesdayMealsEntries);

        //Thursday ListViews
        ThursdayWorkoutsView = (ListView) findViewById(R.id.ThursdayWorkoutsEntries);
        ThursdayMealsView = (ListView) findViewById(R.id.ThursdayMealsEntries);

        //Friday ListViews
        FridayWorkoutsView = (ListView) findViewById(R.id.FridayWorkoutsEntries);
        FridayMealsView = (ListView) findViewById(R.id.FridayMealsEntries);

        //Saturday ListViews
        SaturdayWorkoutsView = (ListView) findViewById(R.id.SaturdayWorkoutsEntries);
        SaturdayMealsView = (ListView) findViewById(R.id.SaturdayMealsEntries);

        //Sunday ListViews
        SundayWorkoutsView = (ListView) findViewById(R.id.SundayWorkoutsEntries);
        SundayMealsView = (ListView) findViewById(R.id.SundayMealsEntries);

        //Monday Arrays
        MondayWorkoutsArray = new ArrayList<String>();
        MondayMealsArray = new ArrayList<String>();
        MondayWorkoutsArray.add("Example Workout 1");
        MondayMealsArray.add("Example Meal 1");

        //Tuesday Arrays
        TuesdayWorkoutsArray = new ArrayList<String>();
        TuesdayMealsArray = new ArrayList<String>();
        TuesdayWorkoutsArray.add("Example Workout 1");
        TuesdayMealsArray.add("Example Meal 1");

        //Wednesday Arrays
        WednesdayWorkoutsArray = new ArrayList<String>();
        WednesdayMealsArray = new ArrayList<String>();
        WednesdayWorkoutsArray.add("Example Workout 1");
        WednesdayMealsArray.add("Example Meal 1");

        //Thursday Arrays
        ThursdayWorkoutsArray = new ArrayList<String>();
        ThursdayMealsArray = new ArrayList<String>();
        ThursdayWorkoutsArray.add("Example Workout 1");
        ThursdayMealsArray.add("Example Meal 1");

        //Friday Arrays
        FridayWorkoutsArray = new ArrayList<String>();
        FridayMealsArray = new ArrayList<String>();
        FridayWorkoutsArray.add("Example Workout 1");
        FridayMealsArray.add("Example Meal 1");

        //Saturday Arrays
        SaturdayWorkoutsArray = new ArrayList<String>();
        SaturdayMealsArray = new ArrayList<String>();
        SaturdayWorkoutsArray.add("Example Workout 1");
        SaturdayMealsArray.add("Example Meal 1");

        //Sunday Arrays
        SundayWorkoutsArray = new ArrayList<String>();
        SundayMealsArray = new ArrayList<String>();
        SundayWorkoutsArray.add("Example Workout 1");
        SundayMealsArray.add("Example Meal 1");

        //Monday Adapters
        MondayWorkoutsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, MondayWorkoutsArray);
        MondayMealsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, MondayMealsArray);
        MondayWorkoutsView.setAdapter(MondayWorkoutsAdapter);
        MondayMealsView.setAdapter(MondayMealsAdapter);

        //Tuesday Adapters
        TuesdayWorkoutsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, TuesdayWorkoutsArray);
        TuesdayMealsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, TuesdayMealsArray);
        TuesdayWorkoutsView.setAdapter(TuesdayWorkoutsAdapter);
        TuesdayMealsView.setAdapter(TuesdayMealsAdapter);

        //Wednesday Adapters
        WednesdayWorkoutsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, WednesdayWorkoutsArray);
        WednesdayMealsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, WednesdayMealsArray);
        WednesdayWorkoutsView.setAdapter(WednesdayWorkoutsAdapter);
        WednesdayMealsView.setAdapter(WednesdayMealsAdapter);

        //Thursday Adapters
        ThursdayWorkoutsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ThursdayWorkoutsArray);
        ThursdayMealsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ThursdayMealsArray);
        ThursdayWorkoutsView.setAdapter(ThursdayWorkoutsAdapter);
        ThursdayMealsView.setAdapter(ThursdayMealsAdapter);

        //Friday Adapters
        FridayWorkoutsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, FridayWorkoutsArray);
        FridayMealsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, FridayMealsArray);
        FridayWorkoutsView.setAdapter(FridayWorkoutsAdapter);
        FridayMealsView.setAdapter(FridayMealsAdapter);

        //Saturday Adapters
        SaturdayWorkoutsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, SaturdayWorkoutsArray);
        SaturdayMealsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, SaturdayMealsArray);
        SaturdayWorkoutsView.setAdapter(SaturdayWorkoutsAdapter);
        SaturdayMealsView.setAdapter(SaturdayMealsAdapter);

        //Sunday Adapters
        SundayWorkoutsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, SundayWorkoutsArray);
        SundayMealsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, SundayMealsArray);
        SundayWorkoutsView.setAdapter(SundayWorkoutsAdapter);
        SundayMealsView.setAdapter(SundayMealsAdapter);

        //Monday Click Listeners
        MondayAddWorkout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                MondayWorkoutsArray.add("New Workout");
                MondayWorkoutsAdapter.notifyDataSetChanged();
            }
        });

        MondayAddMeal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                MondayMealsArray.add("New Meal");
                MondayMealsAdapter.notifyDataSetChanged();
            }
        });

        //Tuesday Click Listeners
        TuesdayAddWorkout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                TuesdayWorkoutsArray.add("New Workout");
                TuesdayWorkoutsAdapter.notifyDataSetChanged();
            }
        });

        TuesdayAddMeal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                TuesdayMealsArray.add("New Meal");
                TuesdayMealsAdapter.notifyDataSetChanged();
            }
        });

        //Wednesday Click Listeners
        WednesdayAddWorkout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                WednesdayWorkoutsArray.add("New Workout");
                WednesdayWorkoutsAdapter.notifyDataSetChanged();
            }
        });

        WednesdayAddMeal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                WednesdayMealsArray.add("New Meal");
                WednesdayMealsAdapter.notifyDataSetChanged();
            }
        });

        //Thursday Click Listeners
        ThursdayAddWorkout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                ThursdayWorkoutsArray.add("New Workout");
                ThursdayWorkoutsAdapter.notifyDataSetChanged();
            }
        });

        ThursdayAddMeal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                ThursdayMealsArray.add("New Meal");
                ThursdayMealsAdapter.notifyDataSetChanged();
            }
        });

        //Friday Click Listeners
        FridayAddWorkout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                FridayWorkoutsArray.add("New Workout");
                FridayWorkoutsAdapter.notifyDataSetChanged();
            }
        });

        FridayAddMeal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                FridayMealsArray.add("New Meal");
                FridayMealsAdapter.notifyDataSetChanged();
            }
        });

        //Saturday Click Listeners
        SaturdayAddWorkout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SaturdayWorkoutsArray.add("New Workout");
                SaturdayWorkoutsAdapter.notifyDataSetChanged();
            }
        });

        SaturdayAddMeal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SaturdayMealsArray.add("New Meal");
                SaturdayMealsAdapter.notifyDataSetChanged();
            }
        });

        //Sunday Click Listeners
        SundayAddWorkout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SundayWorkoutsArray.add("New Workout");
                SundayWorkoutsAdapter.notifyDataSetChanged();
            }
        });

        SundayAddMeal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SundayMealsArray.add("New Meal");
                SundayMealsAdapter.notifyDataSetChanged();
            }
        });

//        Allows individual list entries to be clicked
        MondayWorkoutsView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView <?> a, View v, int position, long id) {
                Toast.makeText(MainActivity.this, "Clicked", Toast.LENGTH_LONG).show();
            }
        });

        MondayMealsView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView <?> a, View v, int position, long id) {
                Toast.makeText(MainActivity.this, "Clicked", Toast.LENGTH_LONG).show();
            }
        });
    }
}
